FROM jupyter/r-notebook:latest

MAINTAINER Muhammad Shahbaz version: 0.1

USER root

VOLUME /home/jovyan/work/

RUN apt-get update && \
    pip install --upgrade pip && \
    pip install pandas matplotlib seaborn statsmodels joblib bitstring && \
    Rscript -e 'install.packages(c("Hmisc", "ggplot2", "data.table", "tibble", "plotly", "bit64"), repos="http://cran.us.r-project.org")' && \
    apt-get -y install libreadline-dev && \
    pip install rpy2

EXPOSE 8888
