# Making ggplot accessible to all!

## Background
Learning ggplot has always been one of my top-five goals since the second year 
of my Ph.D.; a stage in my graduate life when I started to fully appreciate the 
importance of presenting clear-and-concise information to the readers of my 
research papers, both in writing and figures.

However, the idea of learning R, and that being the only way to access ggplot, 
deterred me from learning and using ggplot for a long time. Being an avid user 
of languages, like Python and C, I found it hard to perform even the basic 
operations in R. It's not that I didn't like learning new languages; rather the 
incentives were not there for me to learn this new language.

ggplot is itself a language, embedded within R, with its own 
[grammar](https://psu-psychology.github.io/r-bootcamp-2019/talks/ggplot_grammar.html). 
If my goal is to learn ggplot, I should just do that without understanding the 
intricacies of the parent language (R) that engulfs it. 

I put this goal aside for a couple of years, until I get to work with a fantastic 
researcher, [Lalith Suresh](https://research.vmware.com/researchers/lalith-suresh), 
near the end of my Ph.D. He was an expert in R and showed
me how easy and awesome it is to express plots in ggplot---it was exciting for me 
as I was seeing my own results being plotted in R/ggplot. 

Going through a couple of examples with him, I found that ggplot itself is not 
that hard to learn, but it's the code before that (i.e., for _cleaning and sorting_ 
the dataset) which is the difficult part. (Here's an example of how dense it can 
be in R, [link](https://github.com/Elmo-MCast/numerical-evals/blob/master/notebooks/1-26-2018/baseerat-1M-plots%20(R).ipynb).) 
Once you have sorted your dataset in the proper format, it's relatively intuitive 
and straightforward to plot the dataset using ggplot.

> Generating graphs has two parts: (1) cleanup and sorting of the dataset, (2) 
> plotting the dataset.

This gave me an idea, why not I do _cleanup and sorting_ in Python and _plotting_ 
using ggplot in R. Initially, I started by writing separate code in Python for 
sorting the dataset and then loading it in R to plot it. However, soon this 
process of going back-and-forth between Python and R became very tedious and 
time-consuming. 

I, therefore, started looking for options where I can seamlessly move datasets between Python 
and R, and it is then when I stumbled upon `rpy2` package for Jupyter. It was 
doing exactly what I was looking for: moving objects and data structures seamlessly 
between Python to R at runtime; a pandas data frame would become a native data 
frame in R. And, with Jupyter notebooks, you can move these objects between 
different cells using the [%%R](https://ipython.org/ipython-doc/2/config/extensions/rmagic.html) 
magic command.

I have been using this setup for about a year now. It has allowed me, a programmer 
with limited R knowledge, to use ggplot with ease and speed. How cool is that!

I hope that you will find it equally useful!

Please read further on how to use Python+ggplot. I have hosted a docker image 
([jypyter-rpy2](https://hub.docker.com/r/mshahbaz/jupyter-rpy2?utm_source=docker4mac_2.3.0.3&utm_medium=repo_open&utm_campaign=referral)), 
on [https://hub.docker.com](https://hub.docker.com), and an example to 
get you started quickly.

## Usage

### Clone the `jupyter-rpy2` reposiroty.

```
cd ~/
git clone https://gitlab.com/mshahbaz-dockers/jupyter-rpy2.git
cd jupyter-rpy2
```

### Run the `jupyter-rpy2` docker image under `~/jupyter-rpy2` folder.

```
docker run --rm -it -v $(pwd):/home/jovyan/work -p 8888:8888 mshahbaz/jupyter-rpy2
```

Access the Jupyter portal using the url from the command-line output of the docker run, above.

### Access the example notebook.

You can then run and test the example notebook (example/python-R-notebook.ipynb), located under the `work/example` folder on the `jupyter` portal.

Here are some more examples from the [Lambda-NIC](https://gitlab.com/mshahbaz/mshahbaz.gitlab.io/-/raw/master/publications/icdcs20-lambdanic.pdf) paper: https://gitlab.com/lambda-nic/evaluation.

Enjoy!
